import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Function
from tqdm import tqdm, tqdm_notebook

#from group_equivariant.feature_map.lifting_conv import LiftingConvolution
from group_equivariant.groupy.gconv.splitgconv2d import P4ConvZ2, P4ConvP4 #g conv
from functools import partial # projection layer


# Components    
class GBatchNorm(nn.Module):
    def __init__(self, out_channels, input_stabilizer_size= 4, output_stabilizer_size= 4):
        super().__init__()
        self.out_channels = out_channels
        self.input_stabilizer_size = input_stabilizer_size
        self.output_stabilizer_size = output_stabilizer_size
        self.bn = nn.BatchNorm2d(out_channels* input_stabilizer_size)
    
    def forward(self, x):
        input_shape= x.size()
        input = x.view(input_shape[0], input_shape[1]*self.input_stabilizer_size, input_shape[-2], input_shape[-1])
        y = self.bn(input)  
        batch_size, _, ny_out, nx_out = y.size()     
        y = y.view(batch_size, self.out_channels, self.output_stabilizer_size, ny_out, nx_out)
        
        return y
    

class GMaxPool(nn.Module):
    def __init__(self,k,  input_stabilizer_size= 4, output_stabilizer_size= 4):
        super().__init__()

        self.input_stabilizer_size = input_stabilizer_size
        self.output_stabilizer_size = output_stabilizer_size
        self.mp = nn.MaxPool2d(k)
    
    def forward(self, x):
        input_shape= x.size()
        input = x.view(input_shape[0], input_shape[1]*self.input_stabilizer_size, input_shape[-2], input_shape[-1])
        y = self.mp(input)  
        batch_size, _, ny_out, nx_out = y.size()     
        y = y.view(batch_size, input_shape[1], input_shape[2], ny_out, nx_out)
        
        return y
        
        
    


class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None, in_group='C4'):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels
            
        if in_group=="Z2":
            gconv=P4ConvZ2
            #print('Z2')
        else:
            gconv=P4ConvP4
            #print('C4') 

        self.double_conv = nn.Sequential(
            gconv(in_channels, mid_channels, kernel_size=3, padding=1),
            GBatchNorm(mid_channels),# (N,C,D,H,W) https://pytorch.org/docs/stable/generated/torch.nn.BatchNorm3d.html
            nn.ReLU(inplace=True),
            P4ConvP4(mid_channels, out_channels, kernel_size=3, padding=1),
            GBatchNorm(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            GMaxPool(2),
            DoubleConv(in_channels, out_channels)
        )

    def forward(self, x):
        #print('maxpool_conv:',x.shape)
        return self.maxpool_conv(x)


class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels , in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)


    def forward(self, x1, x2):
        #ex.:
        #up: x1torch.Size([1, 256, 60, 45]) x2torch.Size([1, 256, 120, 90])
        #after torch.Size([1, 256, 120, 90])
        #end up cat:  torch.Size([1, 512, 120, 90])
        
        #print('Upscaling', x1.shape, x2.shape)
        input_shape1 = x1.size()
        input_shape2 = x2.size()
        input1 = x1.view(input_shape1[0], input_shape1[1]*input_shape1[2], input_shape1[-2], input_shape1[-1])
        input2 = x2.view(input_shape2[0], input_shape2[1]*input_shape2[2], input_shape2[-2], input_shape2[-1])
        
        #x = x1.permute(0,2,1,3,4)
        x1 = self.up(input1)
        #x1 = x1.reshape(-1,x1.shape[0], x1.shape[1], x1.shape[2], x1.shape[3]) #.permute(0,1,2,3,4)
        #print("after up", x1.shape)
        # input is CHW (CGHW)
        diffY = input2.size()[2] - input1.size()[2]
        diffX = input2.size()[3] - input1.size()[3]

        input1 = F.pad(input1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        #print('x2' , input2.shape, input1.shape)
        
        x = torch.cat([input2, input1], dim=1)
        #
        
        #print('cat1',x.size())  
        
        batch_size,_, ny_out, nx_out = x.size() 
        x = x.view(batch_size, input_shape2[1]*2, input_shape2[2], ny_out, nx_out)
        #print('cat2 upsampling',x.shape)
        return self.conv(x)
    
class AvgPool(nn.Module):


    def __init__(self):
        super().__init__()

    def forward(self, x):
        shape = x.shape
        mean_per_group = torch.mean(x, 2)
        #print('avglobalppool', mean_per_group.shape)
        return mean_per_group


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)

# Eval

class DiceCoeff(Function):
    """Dice coeff for individual examples"""

    def forward(self, input, target):
        self.save_for_backward(input, target)
        eps = 0.0001
        self.inter = torch.dot(input.view(-1), target.view(-1))
        self.union = torch.sum(input) + torch.sum(target) + eps

        t = (2 * self.inter.float() + eps) / self.union.float()
        return t

    # This function has only a single output, so it gets only one gradient
    def backward(self, grad_output):

        input, target = self.saved_variables
        grad_input = grad_target = None

        if self.needs_input_grad[0]:
            grad_input = grad_output * 2 * (target * self.union - self.inter) \
                         / (self.union * self.union)
        if self.needs_input_grad[1]:
            grad_target = None

        return grad_input, grad_target


def dice_coeff(input, target):
    """Dice coeff for batches"""
    if input.is_cuda:
        s = torch.FloatTensor(1).cuda().zero_()
    else:
        s = torch.FloatTensor(1).zero_()

    for i, c in enumerate(zip(input, target)):
        s = s + DiceCoeff().forward(c[0], c[1])

    return s / (i + 1)


def eval_net(net, loader, device):
    """Evaluation without the densecrf with the dice coefficient"""
    net.eval()
    mask_type = torch.float32 if net.n_classes == 1 else torch.long
    n_val = len(loader)  # the number of batch
    tot = 0

    with tqdm(total=n_val, desc='Validation round', unit='batch', leave=False) as pbar:
        for batch in loader:
            imgs, true_masks = batch['image'], batch['mask']
            imgs = imgs.to(device=device, dtype=torch.float32)
            true_masks = true_masks.to(device=device, dtype=mask_type)

            with torch.no_grad():
                mask_pred = net(imgs)

            if net.n_classes > 1:
                tot += F.cross_entropy(mask_pred, true_masks).item()
            else:
                pred = torch.sigmoid(mask_pred)
                pred = (pred > 0.5).float()
                print('dice', pred.unique(return_counts=True))
                print(true_masks.unique(return_counts=True))
                tot += dice_coeff(pred, true_masks).item()
            pbar.update()

    net.train()
    return tot / n_val


# ======== G-UNET

class GUNet(nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=True):
        super(UNet, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.bilinear = bilinear

            
        #TODO get the inputs for the new layers
        self.inc = DoubleConv(in_channels=3, out_channels=32, in_group="Z2") 
        
        # TODO change conv to gconv and apply to down and up steps
        self.down1 = Down(32, 64)
        self.down2 = Down(64, 128)
        self.down3 = Down(128, 256)
        factor = 2 if bilinear else 1
        self.down4 = Down(256, 512 // factor)
        self.up1 = Up(512, 256 // factor, bilinear)
        self.up2 = Up(256, 128 // factor, bilinear)
        self.up3 = Up(128, 64 // factor, bilinear)
        self.up4 = Up(64, 32, bilinear)
        
        #To create the final segmentation image, we must transform the domain of the feature maps in the U-Net from G back to Z 2 . 
        # To do so, we average the feature map for each filter over rotations. 
        self.projection_layer =AvgPool() #partial(torch.mean, dim=(-3, -2, -1)) # Create the projection layer.
        self.outc = OutConv(32, n_classes)#torch.nn.Linear(32, n_classes)
        

    def forward(self, x):
       
        
        x1 = self.inc(x)  #1st layer instead of pixeis we need to have group equivariant : z2->C4
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        x= self.projection_layer(x)
        #print(x.shape)
        logits = self.outc(x)
        print(logits.unique(return_counts=True))
        print(logits.max())
        print(logits.min())
        return logits
